const express = require('express');
const app = express();
const http = require('http');
const cors = require('cors');
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const socketio = require('socket.io');
const messages = require('./routes/messages');
let messageArray= [];

app.use(express.static(__dirname + '/resources'));
app.use(bodyParser.json());
app.use(cors());
// app.use('/messages', messages);

const server = app.listen(port, () => {
    console.log(`example program started in port ${port}`);
});

const io = socketio.listen(server);
io
.of('/rooms')
.on("connection", (socket) => {
    console.log('CONNECTED');
    let messageArray = [];
    socket.on('joinRoom', (socketRoom) => {
        console.log(socketRoom);
        socket.join(socketRoom.pin);
        // messageArray.push({name, message});
        socket.to(socketRoom.pin).broadcast.emit('user-connected', socketRoom.name);
    })
    socket.on('send-message', (socketRoom) => {
        console.log(socketRoom);
        socket.to(socketRoom.pin).broadcast.emit('message', { name: socketRoom.name, message: socketRoom.message})
    })
});

module.exports = io;

const users = require('./routes/users');
const rooms = require('./routes/rooms');

app.use('/users', users);
app.use('/rooms', rooms);

app.post('/messages', (req, res) =>{
    res.send(messageArray);
})
