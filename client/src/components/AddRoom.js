import React, { Component } from 'react';
import {addRoom} from './RoomFunctions';

class AddRoom extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            error: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        console.log(e);
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const room = {
            name: this.state.name
        }
        addRoom(room).then(res => {
            console.log(res);
            if(res.error) {
                this.setState({
                    error: res.error
                })
            } else {
                this.setState({
                    error: ''
                })
                this.props.history.push(`/rooms/${res.room.pin}`);
            }
        })
    }

    render() {
        return(
            <div className="row">
                <div className="col-md-6 mt-5 mx-auto">
                    <form noValidate onSubmit={this.onSubmit}>
                        <h1>Add a new room</h1>
                        {this.state.error === '' ? '' : <p style={{color: "red"}}>{this.state.error}</p>}
                        <div className="form-group">
                            <label htmlFor="name">Room Name</label>
                            <input type="text"
                            className="form-control"
                            name="name"
                            placeholder="Enter Room Name"
                            value={this.state.name}
                            onChange={this.onChange}
                            />
                        </div>
                        <button type="submit" className="btn btn-lg btn-primary btn-block">
                            Add Room
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default AddRoom;
