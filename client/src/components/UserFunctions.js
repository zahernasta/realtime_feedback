import axios from 'axios'

export const register = (newUser) => {
    return axios.post('users/register', {
        username: newUser.username,
        email: newUser.email,
        password: newUser.password,
        isProfessor: newUser.isProfessor
    })
    .then(res => {
        if(res.data.error) {
            return res.data
        }
        console.log("successfully registered");
        return res.data
    })
    .catch(err => {
        console.log(err);
    })
}


export const login = (user) => {
    console.log(user);
    return axios.post('users/login', {
        username: user.username,
        password: user.password
    })
    .then(res => {
        if(res.data.error) {
            return res.data;
        }
        localStorage.setItem('usertoken', res.data);
        return res.data

    })
    .catch(err => {
        console.log(err);
    })
}

export const deleteUser = (userDeleted) => {
    const id = userDeleted.id;
    const url = 'users/delete/' + id;
    localStorage.removeItem('usertoken');
    return axios.delete(url)
    .then(res => {
        console.log("User deleted successfully");
    })
    .catch(err => {
        console.log(err);
    })
}
