import React, { Component } from 'react';
import {deleteUser} from './UserFunctions';
import jwt_decode from 'jwt-decode';
import socketIoClient from 'socket.io-client';

class Profile extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            email: '',
            professor: '',
            endpoint: 'http://127.0.0.1:3000',
            response: undefined,
        }
    }

    deleteUserFunction(e) {
        e.preventDefault();

        const token = localStorage.usertoken;
        const decoded = jwt_decode(token);
        const user = {
            id: decoded.id
        }

        deleteUser(user).then(res => {
            this.props.history.push('/login');
        });
    }

    componentDidMount() {
        const token = localStorage.usertoken;
        const decoded = jwt_decode(token);
        console.log(decoded);
        this.setState({
            id: decoded.id,
            username: decoded.username,
            email: decoded.email,
            professor: decoded.isProfessor
        })

        const {endpoint} = this.state;
        const socket = socketIoClient.connect(endpoint)
        socket.on("countUpdated", data => {
            this.setState({response: data});
        })

    }

    logOut(e) {
        e.preventDefault();
        localStorage.removeItem('usertoken');
        this.props.history.push('/login');
    }

    render() {
        const {response} = this.state;
        if(localStorage.isProfessor == 'yes') {

        }
        return (
            <div className="container">
                <div className="jumbotron mt-5" style={{backgroundColor: "#FFF"}}>
                    <div className="col-sm-8 mx-auto mb-5">
                        <h1 className="text-center">PROFILE</h1>
                    </div>
                    <table className="table col-md-6 mx-auto">
                        <tbody>
                            <tr>
                                <td>Username</td>
                                <td>{this.state.username}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{this.state.email}</td>
                            </tr>
                            <tr>
                                <td>Professor</td>
                                <td>{this.state.professor}</td>
                            </tr>
                        </tbody>
                        <button type="button" className="btn btn-danger"
                                onClick={this.deleteUserFunction.bind(this)}>Delete User</button>
                        <button type={"button"} className={"btn btn-primary"}
                            onClick={this.logOut.bind(this)}>Log Out</button>
                    </table>
                    <h1>{response}</h1>
                </div>
            </div>
        )
    }
}

export default Profile;
