import React, { Component } from 'react'
import {register} from './UserFunctions'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            email:'',
            password: '',
            isProfessor: '',
            error: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const user = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            isProfessor: this.state.isProfessor
        }

        register(user).then(res => {
            if(res.error) {
                this.setState({
                    error: res.error
                })
            } else {
                this.setState({
                    error: ''
                })
                this.props.history.push('/login');
            }
        })
    }

    render() {
        return(
            <div className="row">
                <div className="col-md-6 mt-5 mx-auto">
                    <form noValidate onSubmit={this.onSubmit}>
                        <h1>Please Register</h1>
                        {this.state.error === '' ? '' : <p style={{color: "red"}}>{this.state.error}</p>}
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text"
                            className="form-control"
                            name="username"
                            placeholder="Enter Username"
                            value={this.state.username}
                            onChange={this.onChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email"
                            className="form-control"
                            name="email"
                            placeholder="Enter email"
                            value={this.state.email}
                            onChange={this.onChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password"
                            className="form-control"
                            name="password"
                            placeholder="Enter Password"
                            value={this.state.password}
                            onChange={this.onChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="isProfessor">Is he a Professor? </label>
                            <select onChange={ this.onChange } name="isProfessor">
                            <option disabled selected value> -- select an option -- </option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-lg btn-primary btn-block">
                            Sign up
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Register;
