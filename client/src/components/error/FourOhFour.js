import React, { Component } from 'react'

class FourOhFour extends Component {

    render() {
        return (
            <h1>404 not Found</h1>
        )
    }
}

export default FourOhFour;
