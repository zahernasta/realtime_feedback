import React, { Component } from 'react'
import {login} from './UserFunctions'

class Login extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            error: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const user = {
            username: this.state.username,
            password: this.state.password
        }

        login(user).then(res => {
            if(res.error) {
                this.setState({
                    error: res.error
                })
                // this.props.history.push('/main-page');
            } else {
                this.setState({
                    error: ''
                })
                this.props.history.push('/main-page');
            }
        })
    }

    render() {
        return(
            <div className="row">
                <div className="col-md-6 mt-5 mx-auto">
                    <form noValidate onSubmit={this.onSubmit}>
                        <h1>Please Sign in</h1>
                        {this.state.error === '' ? '' : <p style={{color: "red"}}>{this.state.error}</p>}
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text"
                            className="form-control"
                            name="username"
                            placeholder="Enter Username"
                            value={this.state.username}
                            onChange={this.onChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password"
                            className="form-control"
                            name="password"
                            placeholder="Enter Password"
                            value={this.state.password}
                            onChange={this.onChange}/>
                        </div>
                        <button type="submit" className="btn btn-lg btn-primary btn-block">
                            Sign in
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;
