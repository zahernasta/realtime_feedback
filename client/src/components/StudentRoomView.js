import React, { Component } from 'react';
import { getRoomByPin } from './RoomFunctions';
import socketio from 'socket.io-client';
import jwt_decode from 'jwt-decode';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const socket = socketio('http://127.0.0.1:3000/rooms');

class RoomView extends Component {
    constructor() {
        super();
        this.state = {
            pin: '',
            roomName: '',
            name: '',
            message: '',
            chat: [],
        }
    }

    componentDidMount() {
        console.log(localStorage);
        const token = localStorage.usertoken;
        const decoded = jwt_decode(token);
        console.log(decoded);
        getRoomByPin(this.props.match.params.pin)
        .then(room => {
            if(!room.name) {
                this.props.history.push('/error');
            } else {
                console.log(room);
                this.setState({
                    pin: room.pin,
                    name: decoded.username,
                    roomName: room.name,
                })
                const {name, pin} = this.state
                socket.emit('joinRoom', {pin, name});
                console.log(this.state.pin);
                socket.on('user-connected', (name, message) => {
                    message = 'connected'
                    this.setState({
                       chat: [...this.state.chat,{name, message}],
                    })
                })

                socket.on('message', (data) => {
                    this.setState({
                        chat: [...this.state.chat, {name: data.name, message: data.message}]
                    })
                })
            }
        })
        .catch(err => {
            console.log(err);
        })
    }

    async onButtonPressed(e) {
        await this.setState({
            [e.target.name]: e.target.value
        })
        const {name, message, pin} = this.state;
        socket.emit('send-message', {name, message, pin});
        this.setState({
            message: ''
        })
    }

    onMessageSubmit = () => {
        const {name, message, pin} = this.state;
        console.log({name, message});
        socket.emit(`send-message`, {name, message, pin});
        this.setState({
            chat: [...this.state.chat, {name: 'You', message}],
            message: ''
        });
    }

    renderChat() {
        const { chat } = this.state;
        return chat.map(({name, message}, idx)=> {
            return(
            <div key={idx}>
                <span className="ml-2" style={{color: "green"}}>{name}: </span>
                <span style={{color: "black"}}>{message}</span>
            </div>
            )
        })
    }

    render() {
        return(
            <Container fluid={true} className="mt-5">
                <h1 className="text-center mb-5">Insert Your Feedback</h1>
                <Row>
                    <Col md={6}>
                    <Card bg="success" text="white" style={{ width: '100%', height:'300px' }}>
                        <Card.Header>Happy</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-center" >Happy Card</Card.Title>
                        <Card.Text className="text-center">
                            <input type="button" name="message"
                            style={{width: '100%', height:'150px'}}
                            className="btn btn-success" onClick={e => this.onButtonPressed(e)} value="happy"></input>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <br/>
                    </Col>
                    <Col md={6}>
                    <Card bg="danger" text="white" style={{ width: '100%', height:'300px '}}>
                        <Card.Header>Frowny</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-center" >Frowny Card</Card.Title>
                        <Card.Text className="text-center">
                        <input type="button" name="message"
                            style={{width: '100%', height:'150px'}}
                            className="btn btn-danger" onClick={e => this.onButtonPressed(e)} value="frowny"></input>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <br/>
                    </Col>
                </Row>

                <Row>
                    <Col md={6}>
                    <Card bg="warning" text="white" style={{ width: '100%', height:'300px '}}>
                        <Card.Header>Surprised</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-center" >Surprised Card</Card.Title>
                        <Card.Text className="text-center">
                        <input type="button" name="message"
                            style={{width: '100%', height:'150px', fontColor: 'white'}}
                            className="btn btn-warning" onClick={e => this.onButtonPressed(e)} value="surprised"></input>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <br/>
                    </Col>
                    <Col md={6}>
                    <Card bg="dark" text="white" style={{ width: '100%', height:'300px' }}>
                        <Card.Header>Confused</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-center" >Confused Card</Card.Title>
                        <Card.Text className="text-center">
                        <input type="button" name="message"
                            style={{width: '100%', height:'150px'}}
                            className="btn btn-dark" onClick={e => this.onButtonPressed(e)} value="confused"></input>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <br/>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default RoomView;
