import axios from 'axios'
import jwt_decode from 'jwt-decode'

export const addRoom = (newRoom) => {
    const token = localStorage.usertoken;
    const decoded = jwt_decode(token);
    return axios.post('rooms/add', {
        name: newRoom.name,
        user_id: decoded.id
    })
    .then(res => {
        console.log(res);
        if(res.data.error) {
            return res.data
        }
        console.log('Successfully added a room');
        return res.data
    })
    .catch(err => {
        console.log(err);
    })
}

export const getRoomByPin = (pin) => {
    return axios.get(`${pin}`)
    .then(room => {
        return room.data;
    })
    .catch(err => {
        return err;
    })
}

export const getAllRooms = () => {
    return axios.get('rooms/non-hidden-rooms')
    .then(rooms => {
        return rooms;
    })
    .catch(err => {
        return err;
    })
}
