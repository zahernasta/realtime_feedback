import React, { Component } from 'react';
import { getAllRooms } from './RoomFunctions';
import { Link } from 'react-router-dom'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import socketio from 'socket.io-client';

const socket = socketio('http://localhost:3000');

class AllRooms extends Component {
    constructor() {
        super();
        this.state = {
            roomsArray: [],
        }
    }

    componentWillMount() {
        socket.on('room-created', data => {
            this.setState({
                roomsArray: [...this.state.roomsArray, data]
            })
        })
    }

    onTextChange = (e) => {
        console.log(e.target);
        this.setState({
            pin: e.target.value
        });
    }

    componentDidMount() {
        getAllRooms()
        .then(rooms => {
            // console.log(rooms.data)
            this.setState({
                roomsArray: rooms.data
            })
            console.log(this.state.roomsArray[1])
        }).catch(err => {
            console.log(err);
        })
    }

    render() {
        let array = this.state.roomsArray
        let elements = [];
        for(let i = array.length - 1; i >= 0; i--) {
            console.log(array[i]);
            elements.push(<Card>
                            <Card.Header as={"h5"}>#{array[i].id}</Card.Header>
                            <Card.Body>
                            <Card.Title>{array[i].name}</Card.Title>
                                <Card.Text>
                                    To access the room enter the corresponding pin and press the button
                                </Card.Text>
                                {/* <Button variant="primary" >Go to Room</Button> */}
                                <Link to={`rooms/${this.state.pin}`} className="btn btn-primary">Go to Room</Link>
                            </Card.Body>
                        </Card>)
        }

        return(
            <div className="container mt-5 justify-content-center">
                <div className="col-sm-8 mx-auto">
                    <h1 className="text-center">ALL ROOMS</h1>
                </div>
                <input type="text" className="form-control" placeholder="Insert Pin"
                       onChange={e => this.onTextChange(e)} value={this.state.pin}></input>
                <br/>
                <div>{elements}</div>
            </div>
        )
    }
}

export default AllRooms;
