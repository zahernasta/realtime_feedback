import React, {Component} from 'react'
import jwt_decode from 'jwt-decode';
import socketio from 'socket.io-client';
import axios from 'axios';

const socket = socketio.connect('http://127.0.0.1:3000');

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            professor: '',
            message: '',
            chat: [],
        }
    }

    componentDidMount() {
        axios.post('/messages').then(res => {this.setState({chat:  res.data})});
        socket.on("message", ({name, message}) => {
            this.setState({
                chat: [...this.state.chat, { name, message }]
            });
        });
    }

    componentWillMount() {
        const token = localStorage.usertoken;
        const decoded = jwt_decode(token);
        console.log(decoded);
        this.setState({
            professor: decoded.isProfessor,
            name: decoded.username
        })
    }

    onTextChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onMessageSubmit = () => {
        const {name, message} = this.state;
        console.log({name, message});
        socket.emit('message', {name, message});
        this.setState({
            message: ''
        });
    }

    async onButtonPressed(e) {
        await this.setState({
            [e.target.name]: e.target.value
        })
        const {name, message} = this.state;
        socket.emit('message', {name, message});
        this.setState({
            message: ''
        })
    }

    renderChat() {
        const { chat } = this.state;
        return chat.map(({name, message}, idx)=> {
            return(
            <div key={idx}>
                <span className="ml-2" style={{color: "green"}}>{name}: </span>
                <span style={{color: "black"}}>{message}</span>
            </div>
            )
        })
    }


    render() {
        console.log(this.state)
        if(this.state.professor == 'no') {
            return(
                <div>
                    <h1 className="mt-4">Student Interface</h1>
                        <div>
                            <input type="button" name="message" onClick={e => this.onButtonPressed(e)} value="happy"></input>
                            <input type="button" name="message" onClick={e => this.onButtonPressed(e)} value="frowny"></input>
                            <input type="button" name="message" onClick={e => this.onButtonPressed(e)} value="surprised"></input>
                            <input type="button" name="message" onClick={e => this.onButtonPressed(e)} value="confused"></input>
                            <div className="mt-4" style={{border: "1px solid black", width: "100%", height:"500px", overflowY: 'scroll'}}>
                                {this.renderChat()}
                            </div>
                        </div>
                </div>
            )
        } else {
            return(
                <div>
                    <h1 className="mt-4">Professor Interface</h1>
                    <input name="message" onChange={e => this.onTextChange(e)} value={this.state.message}></input>
                    <button onClick={this.onMessageSubmit}>Send</button>
                    <div className="mt-4" style={{border: "1px solid black", width: "100%", height:"500px", overflowY: 'scroll'}}>
                        {this.renderChat()}
                    </div>
                </div>
            )
        }
    }
}

export default MainPage;
