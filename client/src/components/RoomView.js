import React, { Component } from 'react';
import { getRoomByPin } from './RoomFunctions';
import socketio from 'socket.io-client';
import jwt_decode from 'jwt-decode';

const socket = socketio('http://127.0.0.1:3000/rooms');

class RoomView extends Component {
    constructor() {
        super();
        this.state = {
            pin: '',
            roomName: '',
            name: '',
            message: '',
            chat: [],
        }
    }

    componentDidMount() {
        console.log(localStorage);
        const token = localStorage.usertoken;
        const decoded = jwt_decode(token);
        console.log(decoded);
        getRoomByPin(this.props.match.params.pin)
        .then(room => {
            if(!room.name) {
                this.props.history.push('/error');
            } else {
                console.log(room);
                this.setState({
                    pin: room.pin,
                    name: decoded.username,
                    roomName: room.name,
                })
                const {name, pin} = this.state
                socket.emit('joinRoom', {pin, name});
                console.log(this.state.pin);
                socket.on('user-connected', (name, message) => {
                    message = 'connected'
                    this.setState({
                       chat: [...this.state.chat,{name, message}],
                    })
                })

                socket.on('message', (data) => {
                    this.setState({
                        chat: [...this.state.chat, {name: data.name, message: data.message}]
                    })
                })
            }
        })
        .catch(err => {
            console.log(err);
        })
    }

    onTextChange = (e) => {
        console.log(e.target.value);
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onMessageSubmit = () => {
        const {name, message, pin} = this.state;
        console.log({name, message});
        socket.emit(`send-message`, {name, message, pin});
        this.setState({
            chat: [...this.state.chat, {name: 'You', message}],
            message: ''
        });
    }

    renderChat() {
        const { chat } = this.state;
        return chat.map(({name, message}, idx)=> {
            return(
            <div key={idx}>
                <span className="ml-2" style={{color: "green"}}>{name}: </span>
                <span style={{color: "black"}}>{message}</span>
            </div>
            )
        })
    }

    render() {
        return(
            <div className="container">
                <div className="col justify-content-md-center">
                    <div className="mt-5">
                        <h1>{this.state.roomName}</h1>
                        <h2>Game Pin: {this.state.pin}</h2>
                    </div>
                    <div>
                    <input name="message" onChange={e => this.onTextChange(e)} value={this.state.message}></input>
                    <button onClick={this.onMessageSubmit}>Send</button>
                    <div className="mt-4" style={{border: "1px solid black", width: "100%", height:"500px", overflowY: 'scroll'}}>
                        {this.renderChat()}
                    </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default RoomView;
