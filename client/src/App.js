import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import jwt_decode from 'jwt-decode';

import Navbar from './components/Navbar';
import Landing from './components/Landing';
import Login from './components/Login';
import Register from './components/Register';
import Profile from './components/Profile';
import FourOhFour from './components/error/FourOhFour';
import MessagePage from './components/MessagePage';
import AddRoom from './components/AddRoom';
import RoomView from './components/RoomView';
import AllRooms from './components/AllRooms';
import StudentRoomView from './components/StudentRoomView';

class App extends Component {
  constructor() {
    super();

    this.state = {
      professor: '',
    }

  }

  componentWillMount() {
    if(localStorage.usertoken) {
      const token = localStorage.usertoken;
      const decoded = jwt_decode(token);
      console.log(decoded);
      this.setState({
          professor: decoded.isProfessor,
      })
    }
}

  render() {
    return (
      <Router>
        <div className="App">
          <Navbar/>
          <div className="container">
            <Switch>
              <Route exact path="/" component={Landing}>
              </Route>
              <Route exact path="/register" component={Register}>
                {/* {this.state.isLogged === true ? <Redirect to="/main-page" /> : <Register />} */}
              </Route>
              <Route exact path="/login" component={Login}>
                {/* {this.state.isLogged === true ? <Redirect to="/main-page" /> : <Login />} */}
              </Route>
              <Route exact path="/profile" component={Profile}>
              {/* {this.state.isLogged === false ? <Redirect to="/login" /> : <Profile />} */}
              </Route>
              <Route exact path="/main-page" component={AllRooms}>
                {this.state.professor === 'no' ? <Route exact path="/main-page" component={AllRooms}></Route> : <Route exact path="/main-page" component={AddRoom}></Route>}
              {/* {this.state.isLogged === false ? <Redirect to="/login" /> : <MainPage />} */}
              </Route>
              <Route exact path="/rooms/:pin" component={RoomView}>
              {this.state.professor === 'no' ? <Route exact path="/rooms/:pin" component={StudentRoomView}></Route> : <Route exact path="/rooms/:pin" component={RoomView}></Route>}
              </Route>
              {/* <Route exact path="/rooms" component={AllRooms}></Route> */}
              <Route exact={true} path="*" component={FourOhFour}></Route>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
