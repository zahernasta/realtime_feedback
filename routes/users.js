const express = require('express');
const users = express.Router();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const UserController = require('../controller/UserController');
users.use(cors());

process.env.SECRET_KEY = 'no_tellin';

users.post('/register', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let email = req.body.email;
    let isProfessor = req.body.isProfessor;
    console.log(username.length);
    if(username.length === 0 || password.length === 0 || email.length === 0) {
        return res.send({error: 'Please fill all the inputs '})
    } else if(isProfessor != 'yes' && isProfessor != 'no') {
        return res.json({error: 'isProfessor can only be yes or no'});
    } else {
        UserController.findOneUserByUsername(username)
        .then(user => {
            if(!user){
                UserController.findOneUserByEmail(email)
                .then(user => {
                    if(!user) {
                        bcrypt.hash(password, 10, (err, hash) => {
                            password = hash;
                            UserController.insertUser(username, password, email, isProfessor)
                            .then(user => {
                                return res.json({status: user.email + ' registered'});
                            })
                            .catch(err => {
                                return res.send({error: err});
                            });
                        })
                    } else {
                        return res.json({error: 'user already exists'});
                    }
                })
                .catch(err => {
                    return res.send({error: err});
                });
            } else {
                return res.json({error: "Username already exists"});
            }
        })
    }
});

users.post('/login', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    UserController.findOneUserByUsername(username)
    .then(user => {
        if(!user) {
            return res.json({error: "User with this username has not been found"})
        } else {
            if(bcrypt.compareSync(password, user.password)) {
                const payload = {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    isProfessor: user.isProfessor
                }
                let jsonToken = jwt.sign(payload, process.env.SECRET_KEY);
                return res.send(jsonToken);
            } else {
                return res.json({error: "Wrong password"});
            }
        }
    })
    .catch(err => {
        return res.send(err);
    })
});

users.delete('/delete/:id', (req, res) => {
    const id = req.params.id;
    UserController.deleteUserById(id)
    .then(user => {
        return res.send("User has been deleted successfully");
    })
    .catch(err => {
        return res.send(err);
    })
})

users.get('/dashboard', (req, res) => {
    const decoded = jwt.verify(req.header['authorization'], process.env.SECRET_KEY);

    UserController.findOneUserById(decoded._id)
    .then(user => {
        if(user) {
            return res.json(user);
        } else {
            return res.send("You don't have access here :/");
        }
    })
    .catch(err => {
        return res.send(err);
    })
})



module.exports = users;
