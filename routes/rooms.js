const express = require('express');
const rooms = express.Router();
const cors = require('cors');
const io = require('../server');


const RoomController = require('../controller/RoomController');
rooms.use(cors());


rooms.get('/all-rooms', (req, res) => {
    RoomController.getAllRooms()
    .then(rooms => {
        res.send(rooms);
    })
    .catch(err => {
        res.send('No rooms found');
    })
});

rooms.get('/non-hidden-rooms', (req, res) => {
    RoomController.getNonHiddenRooms()
    .then(rooms => {
        res.send(rooms)
    })
    .catch(err => {
        res.send('No Rooms found');
    })
});

rooms.get('/:pin', (req, res) => {
    const pin = req.params.pin;
    RoomController.getRoomByPin(pin)
    .then(room => {
        return res.send(room);
    })
    .catch(err => {
        res.send({error: 'An error has occurred'});
    })
})

rooms.post('/add', (req, res) => {
    let pin = Math.floor(Math.random() * 10000);
    let name = req.body.name;
    let user_id = req.body.user_id;

    if(pin.length === 0 || name.length === 0 || user_id.length === 0) {
        return res.send('No field should be empty');
    } else {
        RoomController.getRoomByPin(pin)
        .then(room => {
            if(!room) {
                RoomController.insertRoom(pin, name, user_id)
                .then(room => {
                    io.emit('room-created', room);
                    return res.send({message: 'Room Successfully added', room: room});
                })
                .catch(err => {
                    return res.send({error: 'This name already exists'});
                })
            } else {
                res.send({error: 'Room pin exists, please try again'});
            }
        })
        .catch(err => {
            res.send({error: 'An error has occured'});
        })
    }
});

rooms.put('/update/:id', (req, res) => {
    const id = req.params.id;
    RoomController.updateHiddenRoom(id)
    .then(room => {
        res.send('Room deactivated successfully');
    })
    .catch(err => {
        console.log(err);
        res.send({error: 'An error has occured'});
    })
})

rooms.delete('/delete/:id', (req, res) => {
    const id = req.params.id;
    RoomController.deleteRoom(id)
    .then(success => {
        res.send('Room deleted successfully');
    })
    .catch(err => {
        res.send({error: 'An error has occured'});
    })
});

module.exports = rooms;
