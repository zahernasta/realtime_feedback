const config = require('./config');
const Sequelize = require('sequelize');
const mysql = require('mysql2/promise');

//In cazul in care nu merge din prima, rerulati serverul
mysql.createConnection({
    user : config.databaseOptions.user,
    password : config.databaseOptions.password
})
.then((connection) => {
    return connection.query('CREATE DATABASE IF NOT EXISTS realtime_feedback')
})
.catch((err) => {
    console.warn(err.stack)
})

const sequelize = new Sequelize(config.databaseOptions.database, config.databaseOptions.user,
    config.databaseOptions.password, {
        host: config.databaseOptions.host,
        dialect: 'mysql'
});

const db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
