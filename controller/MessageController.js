const modelMessage = require('../model/Message');

exports.insertMessage = (message, user_id, room_id) => {
    return modelMessage.Message.create({
        message: message,
        user_id: user_id,
        room_id: room_id
    })
}

exports.deleteMessageById = (id) => {
    return modelMessage.Message.destroy({
        where: {
            id: id
        }
    });
}
