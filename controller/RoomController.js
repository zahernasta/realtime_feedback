const modelRoom = require('../model/Room');

exports.getAllRooms = () => {
    return modelRoom.Room.findAll();
}

exports.getNonHiddenRooms = () => {
    return modelRoom.Room.findAll({
        where: {
            hidden: 0
        }
    });
}

exports.getRoomByPin = (pin) => {
    return modelRoom.Room.findOne({
        where: {
            pin: pin
        }
    })
}

exports.getRoomById = (id) => {
    return modelRoom.Room.findOne({
        where: {
            id: id
        }
    })
}

exports.insertRoom = (pin, name, user_id) => {
    return modelRoom.Room.create({
        pin: pin,
        name: name,
        user_id: user_id,
        hidden: 0
    });
}

exports.updateHiddenRoom = (id) => {
    return modelRoom.Room.update({
        hidden: 1
        },
        {
        where: {
           id: id
        }
    });
}

exports.deleteRoom = (id) => {
    return modelRoom.Room.destroy({
        where: {
            id: id
        }
    })
}
