const modelUser = require('../model/User');

exports.insertUser = (username, password, email, isProfessor) => {
    return modelUser.User.create({
        username: username,
        password: password,
        email: email,
        isProfessor: isProfessor
    });
}

exports.findOneUserByEmail = (email) => {
    return modelUser.User.findOne({
        where : {
            email: email,
        }
    });
}

exports.findOneUserByUsername = (username) => {
    return modelUser.User.findOne({
        where: {
            username: username,
        }
    });
}

exports.findOneUserById = (id) => {
    return modelUser.User.findOne({
        where: {
            id: id
        }
    });
}

exports.deleteUserById = (id) => {
    return modelUser.User.destroy({
        where: {
            id: id,
        }
    });
}

exports.verifyEnteredData = (user, res, req, password) => {
    if(!user) {
        res.redirect('/login');
    } else {
        req.session.user = user.dataValues;
        res.redirect('/dashboard');
    }
}
