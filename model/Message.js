const Sequelize = require('sequelize');
const db = require('../db');
const Model = Sequelize.Model;
const sequelize = db.sequelize;

class Message extends Model {}
Message.init({
    message: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        reference: 'users',
        referenceKey: 'id'
    },
    room_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        reference: 'rooms',
        referenceKey: 'id'
    }
}, {
    sequelize,
    modelName: 'message'
});

Message.sync()
        .then(() => { console.log('Messages table created successfully') })
        .catch(() => { console.log(err) });

exports.Message = Message;
