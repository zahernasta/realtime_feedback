const Sequelize = require('sequelize');
const db = require('../db');
const Model = Sequelize.Model;
const sequelize = db.sequelize;
const Message = require('./Message');
const Room = require('./Room');


class User extends Model {}
User.init({
    username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    isProfessor: {
        type: Sequelize.STRING,
        allowNull: false,
    }
}, {
    sequelize,
    modelName: 'user'
});

// User.hasMany(Room);
// User.hasMany(Message);

User.sync()
    .then(() => console.log('table created successfully'))
    .catch((err)=> console.log('oops it fucked up'));

exports.User = User;
