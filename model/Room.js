const Sequelize = require('sequelize');
const db = require('../db');
const Model = Sequelize.Model;
const sequelize = db.sequelize;

class Room extends Model {}
 Room.init({
    pin: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    } ,
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
    },
    hidden: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        reference: 'users',
        referenceKey: 'id'
    },
},
    {
    sequelize,
    modelName: 'room'
 })

Room.sync()
    .then(() => console.log('Room table created successfully'))
    .catch((err) => console.log(err));

exports.Room = Room;
